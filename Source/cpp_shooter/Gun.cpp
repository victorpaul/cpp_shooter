// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"

#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

const FName MuzzleSocketName = TEXT("MuzzleFlashSocket");

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Scene"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun Mesh"));
	Mesh->SetupAttachment(Root);
}

void AGun::PullTrigger()
{
	UE_LOG(LogTemp, Warning, TEXT("Pull trigger"));
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, MuzzleSocketName);
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, MuzzleSocketName);

	FHitResult Hit;
	FVector ShotDirection;
	if (GunTrace(Hit, ShotDirection))
	{
		// DrawDebugPoint(GetWorld(), Hit.Location, 20, FColor::Red, true);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ImpactSound, Hit.Location, ShotDirection.Rotation());
		auto HitActor = Hit.GetActor();
		if (HitActor)
		{
			auto Controller = GetOwnerController();
			FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
			HitActor->TakeDamage(Damage, DamageEvent, Controller, this);
		}
	}
}

void AGun::ReleaseTrigger()
{
	UE_LOG(LogTemp, Warning, TEXT("Release trigger"));
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	auto Controller = GetOwnerController();
	if (!Controller) return false;

	FVector CameraLocation;
	FRotator CameraRotation;
	Controller->GetPlayerViewPoint(CameraLocation, CameraRotation);
	// DrawDebugCamera(GetWorld(), CameraLocation, CameraRotation, 90, 3, FColor::Red, true);
	FVector TargetLocation = CameraLocation + CameraRotation.Vector() * MaxRange;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	auto BulletChannel = ECC_GameTraceChannel1;

	ShotDirection = -CameraRotation.Vector();
	return GetWorld()->LineTraceSingleByChannel(Hit, CameraLocation, TargetLocation, BulletChannel, Params);
}

AController* AGun::GetOwnerController()
{
	auto OwnerPawn = Cast<APawn>(GetOwner());
	if (!OwnerPawn) return nullptr;

	return OwnerPawn->GetController();
}
