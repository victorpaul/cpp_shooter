// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "BTS_UpdateSeeingPLayer.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SHOOTER_API UBTS_UpdateSeeingPLayer : public UBTService_BlackboardBase
{
	GENERATED_BODY()

public:

	UBTS_UpdateSeeingPLayer();

protected:

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
