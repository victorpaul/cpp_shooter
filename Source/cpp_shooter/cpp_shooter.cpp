// Copyright Epic Games, Inc. All Rights Reserved.

#include "cpp_shooter.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, cpp_shooter, "cpp_shooter" );
