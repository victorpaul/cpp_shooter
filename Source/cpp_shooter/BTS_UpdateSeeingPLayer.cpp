// Fill out your copyright notice in the Description page of Project Settings.


#include "BTS_UpdateSeeingPLayer.h"


#include "AIController.h"
#include "ShooterCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

UBTS_UpdateSeeingPLayer::UBTS_UpdateSeeingPLayer()
{
	NodeName = "Update player seeing player";
}

void UBTS_UpdateSeeingPLayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	
	auto Player = Cast<AShooterCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	auto Controller = OwnerComp.GetAIOwner();
	
	if (!Player || !Controller) return;

	if (Controller->LineOfSightTo(Player) && !Player->IsDead())
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), Player);
	}
	else
	{
		OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
	}
}
