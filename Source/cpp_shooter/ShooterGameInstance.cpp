// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterGameInstance.h"

#include "EngineUtils.h"
#include "ShooterAIController.h"
#include "ShooterCharacter.h"

FString UShooterGameInstance::GetGameInfo()
{
	float Fps = 0;
	int EnemiesLeft = 0;
	if (GetWorld())
	{
		Fps = 1 / GetWorld()->GetDeltaSeconds();
		for (auto Enemy : TActorRange<AShooterAIController>(GetWorld()))
		{
			if (!Enemy->IsDead())
			{
				EnemiesLeft++;
			}
		}
	}

	return FString::Printf(TEXT("FPS=%f\nBOTS=%d"), Fps, EnemiesLeft);
}
