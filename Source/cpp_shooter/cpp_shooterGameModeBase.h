// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "cpp_shooterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SHOOTER_API Acpp_shooterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PawnKilled(APawn* KilledPawn);
};
