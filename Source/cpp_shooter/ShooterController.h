// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterController.generated.h"

/**
 * 
 */
UCLASS()
class CPP_SHOOTER_API AShooterController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void GameHasEnded(class AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;

protected:
	virtual void BeginPlay() override;
private:
	UPROPERTY(EditAnywhere)
	float RestartDelay = 5;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> WidgetClassLoseScreen;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> WidgetClassWinScreen;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> WidgetClassHud;

	UPROPERTY()
	UUserWidget* WidgetHud;

	FTimerHandle TimerHandle;
};
