// Fill out your copyright notice in the Description page of Project Settings.


#include "BTS_SetPlayerLocation.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

UBTS_SetPlayerLocation::UBTS_SetPlayerLocation()
{
	NodeName = TEXT("Update player location");
}

void UBTS_SetPlayerLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	auto Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (Player == nullptr) return;

	OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), Player->GetActorLocation());
}
