// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAIController.h"


#include "ShooterCharacter.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"

const FName BB_PlayerLocationKey = TEXT("PlayerLocation");
const FName BB_StartLocationKey = TEXT("StartLocation");
const FName BB_LastKnownPlayerLocationKey = TEXT("LastKnownPlayerLocation");

void AShooterAIController::BeginPlay()
{
	Super::BeginPlay();

	auto Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (!ensure(AIBehaviorTree!=nullptr)) return;

	RunBehaviorTree(AIBehaviorTree);

	GetBlackboardComponent()->SetValueAsVector(BB_StartLocationKey, GetPawn()->GetActorLocation());
}

void AShooterAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool AShooterAIController::IsDead() const
{
	auto ControlledPawn = Cast<AShooterCharacter>(GetPawn());
	if (!ensure(ControlledPawn != nullptr)) return true;
	return ControlledPawn->IsDead();
}
