// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"


#include "cpp_shooterGameModeBase.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	GetMesh()->HideBoneByName(TEXT("weapon_r"), PBO_None);

	Gun = GetWorld()->SpawnActor<AGun>(GunClass);

	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform,TEXT("WeaponSocket"));
	Gun->SetOwner(this);

	CameraBoom = FindComponentByClass<USpringArmComponent>();
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::InputMoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::InputMoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &AShooterCharacter::InputLookUp);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::InputLookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &AShooterCharacter::InputLookRight);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::InputLookRightRate);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("Shoot"), IE_Pressed, this, &AShooterCharacter::InputPullGunTrigger);
	PlayerInputComponent->BindAction(TEXT("Shoot"), IE_Released, this, &AShooterCharacter::InputReleaseGunTrigger);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Pressed, this, &AShooterCharacter::InputStartAiming);
	PlayerInputComponent->BindAction(TEXT("Aim"), IE_Released, this, &AShooterCharacter::InputStopAiming);
}

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (IsDead()) return 0;

	DamageToApply = FMath::Min(Health, DamageToApply);

	Health -= DamageToApply;
	UE_LOG(LogTemp, Warning, TEXT("Health=%f, Damage=%f"), Health, DamageToApply);

	if (IsDead())
	{
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		auto GameMode = GetWorld()->GetAuthGameMode<Acpp_shooterGameModeBase>();
		if (!ensure(GameMode!=nullptr)) return DamageToApply;
		GameMode->PawnKilled(this);

		DetachFromControllerPendingDestroy();
	}

	return DamageToApply;
}

void AShooterCharacter::InputMoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void AShooterCharacter::InputMoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

void AShooterCharacter::InputLookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void AShooterCharacter::InputLookUpRate(float Value)
{
	AddControllerPitchInput(RotationRate * Value * GetWorld()->DeltaTimeSeconds);
}

void AShooterCharacter::InputLookRight(float Value)
{
	AddControllerYawInput(Value);
}

void AShooterCharacter::InputLookRightRate(float Value)
{
	AddControllerYawInput(RotationRate * Value * GetWorld()->DeltaTimeSeconds);
}

void AShooterCharacter::InputPullGunTrigger()
{
	if (!Gun) return;

	Gun->PullTrigger();
}

void AShooterCharacter::InputReleaseGunTrigger()
{
	if (!Gun) return;

	Gun->ReleaseTrigger();
}

void AShooterCharacter::InputStartAiming()
{
	UE_LOG(LogTemp, Warning, TEXT("Start aiming"));

	if (ensure(CameraBoom))
	{
		auto Location = CameraBoom->GetRelativeLocation();
		CameraBoom->SetRelativeLocation(FVector(Location.X, Location.Y * -1, Location.Z));
		CameraBoom->SocketOffset = FVector(CameraBoom->SocketOffset.X, CameraBoom->SocketOffset.Y * -1, CameraBoom->SocketOffset.Z);
	}
}

void AShooterCharacter::InputStopAiming()
{
	UE_LOG(LogTemp, Warning, TEXT("Stop aiming"));
}
