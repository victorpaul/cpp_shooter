// Fill out your copyright notice in the Description page of Project Settings.


#include "KillEmAllGameMode.h"

#include "EngineUtils.h"
#include "ShooterAIController.h"
#include "ShooterController.h"

void AKillEmAllGameMode::PawnKilled(APawn* KilledPawn)
{
	Super::PawnKilled(KilledPawn);

	UE_LOG(LogTemp, Warning, TEXT("AKillEmAllGameMode::PawnKilled"));

	auto PlayerController = Cast<APlayerController>(KilledPawn->GetController());
	if (PlayerController != nullptr)
	{
		EndGame(false);
		return;
	}

	for (auto Controller : TActorRange<AShooterAIController>(GetWorld()))
	{
		if (!Controller->IsDead()) return;
	}

	EndGame(true);
}

void AKillEmAllGameMode::EndGame(bool bIsplayerWin)
{
	for (auto Controller : TActorRange<AController>(GetWorld()))
	{
		auto IsWinner = Controller->IsPlayerController() && bIsplayerWin;
		Controller->GameHasEnded(Controller->GetPawn(), IsWinner);
	}
}
