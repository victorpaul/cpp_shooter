// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"

#include "ShooterCharacter.generated.h"

class AGun;

UCLASS()
class CPP_SHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintPure)
	bool IsDead() const { return Health <= 0; }

	UFUNCTION(BlueprintPure)
	float HealthPercent() const { return Health / MaxHealth; }

	void InputPullGunTrigger();

private:
	UPROPERTY(EditAnywhere)
	float RotationRate = 75;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGun> GunClass;
	UPROPERTY()
	AGun* Gun;
	UPROPERTY(EditDefaultsOnly)
	float MaxHealth = 100;
	UPROPERTY(VisibleAnywhere)
	float Health;
	UPROPERTY()
	USpringArmComponent* CameraBoom;

	void InputMoveForward(float Value);
	void InputMoveRight(float Value);
	void InputLookUp(float Value);
	void InputLookUpRate(float Value);
	void InputLookRight(float Value);
	void InputLookRightRate(float Value);
	void InputReleaseGunTrigger();
	void InputStartAiming();
	void InputStopAiming();
};
