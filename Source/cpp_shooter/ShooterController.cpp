// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterController.h"

#include "Blueprint/UserWidget.h"

void AShooterController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	if (ensure(WidgetHud))
	{
		WidgetHud->RemoveFromViewport();
	}

	if (bIsWinner)
	{
		auto Widget = CreateWidget(this, WidgetClassWinScreen);
		if (!ensure(Widget!= nullptr)) return;
		Widget->AddToViewport();
	}
	else
	{
		auto Widget = CreateWidget(this, WidgetClassLoseScreen);
		if (!ensure(Widget!= nullptr)) return;
		Widget->AddToViewport();
	}

	GetWorldTimerManager().SetTimer(TimerHandle, this, &APlayerController::RestartLevel, RestartDelay);
}

void AShooterController::BeginPlay()
{
	Super::BeginPlay();

	WidgetHud = CreateWidget(this, WidgetClassHud);
	if (ensure(WidgetHud))
	{
		WidgetHud->AddToViewport();
	}
}
